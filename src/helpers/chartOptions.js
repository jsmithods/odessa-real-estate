export const barChartOptions = {
      label: 'Number of items',
      backgroundColor: 'rgba(255,99,132,0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: ''
};

export const lineChartOptions = {
  label: 'Number of items',
  fill: false,
  lineTension: 0.1,
  backgroundColor: 'rgba(75,192,192,0.4)',
  borderColor: 'rgba(75,192,192,1)',
  borderCapStyle: 'butt',
  borderDash: [],
  borderDashOffset: 0.0,
  borderJoinStyle: 'miter',
  pointBorderColor: 'rgba(75,192,192,1)',
  pointBackgroundColor: '#fff',
  pointBorderWidth: 1,
  pointHoverRadius: 5,
  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
  pointHoverBorderColor: 'rgba(220,220,220,1)',
  pointHoverBorderWidth: 2,
  pointRadius: 1,
  pointHitRadius: 10,
  data: ''
};

export const pieChartOptions = {
  data: '',
  backgroundColor: [
    '#FF6384',
    '#36A2EB',
    '#FFCE56',
    '#C39BD3',
    '#76D7C4',
    '#884EA0',
    '#FF6384',
    '#36A2EB',
  ],
  hoverBackgroundColor: [
    '#FF6384',
    '#36A2EB',
    '#FFCE56',
    '#C39BD3',
    '#76D7C4',
    '#884EA0',
    '#FF6384',
    '#36A2EB',
  ]
};