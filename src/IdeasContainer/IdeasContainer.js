import React, { Component } from 'react';
import './IdeasContainer.css';

class IdeasContainer extends Component {
  render() {
    return <div>
      <div className="flex-grid">
        <div>
          <p>Ideas</p>
          <ul>
            <li>Create distinct pages for:  I. general graph (Includes all the possible statistics for whole period)</li>
            <li>II. Single page with statistics for this period</li>
          </ul>
          <p>Details about data graph: GENERAL</p>
          <ul>
            <li>Number of appartments</li>
            <li>Avarage price per square meter (by room count and district). Maybe I need selects.</li>
            <li>Maybe dynamics in % (part of single to all quantity)</li>
            <li>avarge price of appartment (by room and district)</li>
            <li>Maybe I need a help of table to snow the data as a table</li>
          </ul>

          <p>Details about data graph: SINGLE</p>
          <ul>
            <li>Structure (by room count and district)</li>
            <li>Avarage price (by room and district)</li>
            <li>Maybe avarage square meters and avarage kitchen size</li>
          </ul>

          <p>Show appartments of a map (using GMaps). At the same time get coords by street name</p>

          <p>Show some appartment as an item in the catalogue.
            Using general info about app, photos, situation on a map, similar items (by price and in 1 km radium)
          </p>
          <p>Print report as pdf</p>
          <p>Add some widgets like: total amount of apps, rent, houses etc ...</p>

        </div>
      </div>
    </div>;
  }
}

export default IdeasContainer;