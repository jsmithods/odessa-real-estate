import React, { Component } from 'react';
import './SingleFlat.css';

import { Link } from "react-router-dom";

import apartmentsList from '../../data_source/flat_gps_info';
import rawImagePath from '../../data_source/flat_images_info';

class SingleFlat extends Component {
  state = {
    currentFlat: {},
    images: [],
    currentImage: 0
  };

  componentWillMount() {
    this.getApartmentInfo();
    this.getImages();
  }

  getApartmentInfo = () => {
    let flatId = this.props.match.params.flatId;
    let current = apartmentsList.filter(item => {
      return item.uid ===  flatId;
    });
    this.setState({currentFlat: current[0]});
  };

  getImages = () => {
    let flatId = this.props.match.params.flatId;
    let output = {};
    rawImagePath.forEach(item => {
      let fl = item.flatId;
      if(output[fl] === undefined) {
        output[fl] = [];
      }
      output[fl].push(item.imageName);
    });
    this.setState({images: output[flatId]})
  };

  handleMoveImage = (direction) => {
    let oldImage = this.state.currentImage;
    let imageLength = this.state.images.length;
    let nextImage = 0;
    if (direction === 'next') {
      nextImage = oldImage + 1;
    } else {
      nextImage = oldImage - 1;
    }
    if (nextImage === -1) {
      nextImage = imageLength - 1;
    }
    if (nextImage === imageLength) {
      nextImage = 0;
    }

    this.setState({currentImage: nextImage})
  };

  render() {
    const apartmentId = this.state.currentFlat.uid;
    const imgUrl = `http://localhost:9595/static/img/${apartmentId}/${apartmentId}-${this.state.images[this.state.currentImage]}.jpg`;
    // console.log(imgUrl);

    const backButton = <div className="back-button">
      <Link to="/list">Back</Link>
    </div>;

    return <div className="single-flat-container">
      {/*<p>SingleFlat</p>*/}
      {/*<p>ID: {this.props.match.params.flatId}</p>*/}
      {backButton}
      <div className="slideshow-container">
        <img src={imgUrl} alt=""/>
        <a className="prev" onClick={() => this.handleMoveImage('prev')} >&#10094;</a>
        <a className="next" onClick={() => this.handleMoveImage('next')} >&#10095;</a>
      </div>
      <div children="single-flat-info">
        <p>Район: {this.state.currentFlat.districtName}</p>
        <p>Улица: {this.state.currentFlat.streetName}</p>
        <p>Этаж: {this.state.currentFlat.floor}</p>
        <p>Комнат: {this.state.currentFlat.rooms}</p>
        <p>Общая площадь: {this.state.currentFlat.sM}</p>
        <p>Жилая площадь: {this.state.currentFlat.lSM}</p>
        <p>Кухня: {this.state.currentFlat.kSM}</p>
        <p>Цена: {this.state.currentFlat.price}  за метр: {this.state.currentFlat.priceItem} </p>
        <p>{this.state.currentFlat.description}</p>
      </div>
    </div>;
  }
}

export default SingleFlat;