import React, { Component } from 'react';
import './ListContainer.css';

import ListBlockItem from './ListBlockItem/ListBlockItem';

import apartmentsList from '../data_source/flat_gps_info';
import rawImagePath from '../data_source/flat_images_info';

class ListContainer extends Component {
  state = {
    viewMode: "blocks",
    filter: {
      byDistrict: null
    },
    flatList: [],
    imgByFlat: {}
  };

  componentDidMount() {
    this.setState({flatList: apartmentsList});
    this.getImgByFlat();
  }

  getImgByFlat = () => {
    let output = {};
    rawImagePath.forEach(item => {
      let fl = item.flatId;
      if(output[fl] === undefined) {
        output[fl] = [];
      }
      output[fl].push(item.imageName);
    });
    this.setState({imgByFlat: output});
  };

  filterList = (byDistrict) => {
    if(byDistrict) {
      let filteredList = apartmentsList.filter(item => {
        return item.districtName === byDistrict;
      });
      this.setState({flatList: filteredList});
    } else {
      this.setState({flatList: apartmentsList});
    }
  };

  handleFilerDistrict = (event) => {
    let selectedValue = event.target.value;
    this.setState({filter: {byDistrict: selectedValue}});
    this.filterList(selectedValue);
  };

  handleViewModeChange = (e) => {
    this.setState({viewMode: e.currentTarget.value})
  };

  getRndNumber = (length) => {
    return Math.floor(Math.random() * length);
  };

  render() {
    const selectByDistrict = <div>
      <select value={this.state.filter.byDistrict} onChange={this.handleFilerDistrict}>
        <option value="">Show all</option>
        <option value="Аркадия">Аркадия</option>
        <option value="Киевский">Киевский</option>
        <option value="Приморский">Приморский</option>
        <option value="Малиновский">Малиновский</option>
        <option value="Суворовский">Суворовский</option>
      </select>
    </div>;

    const radioViewMode = <div>
      <span>
        table view: <input type="radio" name="site_name"
                           value={"table"}
                           checked={this.state.viewMode === "table"}
                           onChange={this.handleViewModeChange} />
      </span>
      <span>
        blocks view: <input type="radio" name="site_name"
                            value={"blocks"}
                            checked={this.state.viewMode === "blocks"}
                            onChange={this.handleViewModeChange} />
      </span>
    </div>;

    const flatTable = <div className="analyse-table">
      <table>
        <tr>
          <th>Id</th>
          <th>Street</th>
          <th>District</th>
          <th>Rooms</th>
          <th>Square</th>
          <th>Living</th>
          <th>Kitchen</th>
          <th>Price</th>
        </tr>
        {/*/ Iterating over flat items /*/}
        {this.state.flatList.map(item => {
            return <tr>
              <td>{item.uid}</td>
              <td>{item.streetName}</td>
              <td>{item.districtName}</td>
              <td>{item.rooms}</td>
              <td>{item.sM}</td>
              <td>{item.lSM || '-'}</td>
              <td>{item.kSM || '-'}</td>
              <td>{item.price}</td>
            </tr>
          })
        }
      </table>
    </div>;

    const flatBlocks = <div className="flat-block-container">
      {this.state.flatList.map(item => {
        // let imgUrl = 'http://localhost:9595/static/img/13698956/13698956-79438158.jpg';
        // console.log(item);
        // let itemObjectLength = this.state.imgByFlat[item.uid].length;
        // let imgUrl = `http://localhost:9595/static/img/${item.uid}/${item.uid}-${this.state.imgByFlat[item.uid][this.getRndNumber(itemObjectLength)]}.jpg`;
        let imgUrl = `http://localhost:9595/static/img/${item.uid}/${item.uid}-${this.state.imgByFlat[item.uid][0]}.jpg`;
        let blockItem = <ListBlockItem
          uid={item.uid}
          info={{
            imageUrl: imgUrl,
            price: item.price,
            rooms: item.rooms,
            square: item.sM,
            street: item.streetName,
            district: item.districtName
          }}
        />;
        return blockItem
      })}
    </div>;

    const showFlatsMode = this.state.viewMode === "table" ? flatTable : flatBlocks;

    return <div>
      <div className="filter-container">
        {selectByDistrict}
        {radioViewMode}
      </div>
      {showFlatsMode}
    </div>;
  }
}

export default ListContainer;