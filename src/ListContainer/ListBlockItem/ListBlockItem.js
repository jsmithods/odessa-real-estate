import React, { Component } from 'react';
import './ListBlockItem.css';

import { Link } from "react-router-dom";

class ListBlockItem extends Component {
  render() {
    const flatLink = `/list/${this.props.uid}`;
    return <div className="list-block-item">
      <Link to={flatLink}>
        <img src={this.props.info.imageUrl}  alt="photo"/>
      </Link>

      <p>$ {this.props.info.price}</p>
      <p><span>{this.props.info.rooms}к</span> - <span>{this.props.info.square} м2</span></p>
      <p>{this.props.info.street}</p>
      <p>{this.props.info.district}</p>
    </div>;
  }
}

export default ListBlockItem;