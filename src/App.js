import React, { Component } from 'react';
import './App.css';

import AdminLayout from './AdminLayout/AdminLayout';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AdminLayout />
      </div>
    );
  }
}

export default App;
