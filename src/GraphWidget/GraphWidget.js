import React, { Component } from 'react';
import './GraphWidget.css';

import {barChartOptions, lineChartOptions, pieChartOptions} from '../helpers/chartOptions';

// Chart lib imports
import {Bar, Line, Pie} from 'react-chartjs-2';

class GraphWidget extends Component {
  state = {
    header: 'default Header',
    content: 'default Content',
    chartSetings: {
      labels: '',
      datasets: ''
    },
    defaultChart: {
      labels: ['1', '2', '3'],
      data: [100, 250, 80]
    },
    selectParams: {
      district: 0,
      variable: 'sum'
    },
    pieParams: {
      district: 7,
      timestamp: 1544270458
    },
    tableParams: {
      district: 0,
      timestamp: 1544270458
    }
  };

  componentWillMount() {}
  
  composeDataChartObject = (chartType) => {
    let currentOptions = {};
    let chartLabels = {};
    let currentChartData = {};

    // set proper chart options
    switch (chartType) {
      case 'bar':
        currentOptions = Object.assign({}, barChartOptions);
        break;
      case 'line':
        currentOptions = Object.assign({}, lineChartOptions);
        break;
      case 'pie':
        currentOptions = Object.assign({}, pieChartOptions);
        break;
    }

    // set proper chart data
    if (this.props.chartData) {
      currentOptions.data = this.props.chartData.data;
      chartLabels = this.props.chartData.labels;
    } else {
      currentOptions.data = this.state.defaultChart.data;
      chartLabels = this.state.defaultChart.labels;
    }

    // combine elements for final object
    currentChartData = {
      labels: chartLabels,
      datasets: [
        {
          ...currentOptions
        }
      ]
    };

    return currentChartData;
  };

  setRenderedChart = (chartType) => {
    let chartOptions = this.composeDataChartObject(chartType);
    let renderedChart = '';

    switch (chartType) {
      case 'bar':
        renderedChart = <Bar
          data={chartOptions}
          /*width={500}
          height={500}*/
          options={{
            maintainAspectRatio: false
          }}
        />;
        break;
      case 'line':
        renderedChart = <Line data={chartOptions} />;
        break;
      case 'pie':
        renderedChart = <Pie data={chartOptions} />;
        break;
    }
    return renderedChart;
  };

  handleChangeSelectState = (params) => {
    this.props.handleWidgetSelect(params);
  };

  handleSelectDistrictChange = (event) => {
    let chartType = this.props.chartType;
    switch (chartType) {
      case 'bar':
        let barVariable = this.state.selectParams.variable;
        let barSelectParams = {};
        barSelectParams.district = event.target.value;
        barSelectParams.variable = barVariable;
        this.setState({selectParams: barSelectParams});
        this.handleChangeSelectState(barSelectParams);
        break;
      case 'pie':
        let pieTimestamp = this.state.pieParams.timestamp;
        let pieSelectParams = {};
        pieSelectParams.district = event.target.value;
        pieSelectParams.timestamp = pieTimestamp;
        this.setState({pieParams: pieSelectParams});
        this.handleChangeSelectState(pieSelectParams);
        break;
      case 'table':
        let tableTimestamp = this.state.pieParams.timestamp;
        let tableSelectParams = {};
        tableSelectParams.district = event.target.value;
        tableSelectParams.timestamp = tableTimestamp;
        this.setState({tableParams: tableSelectParams});
        this.handleChangeSelectState(tableSelectParams);
        break;
    }
  };

  handleSelectTimestampChange = (event) => {
    let chartType = this.props.chartType;
    switch (chartType) {
      case 'bar':
        let barVariable = this.state.selectParams.variable;
        let barSelectParams = {};
        barSelectParams.district = event.target.value;
        barSelectParams.variable = barVariable;
        this.setState({selectParams: barSelectParams});
        this.handleChangeSelectState(barSelectParams);
        break;
      case 'pie':
        let pieDistrict = this.state.pieParams.district;
        let pieSelectParams = {};
        pieSelectParams.timestamp = event.target.value;
        pieSelectParams.district = pieDistrict;
        this.setState({pieParams: pieSelectParams});
        this.handleChangeSelectState(pieSelectParams);
        break;
      case 'table':
        let tableDistrict = this.state.tableParams.district;
        let tableSelectParams = {};
        tableSelectParams.timestamp = event.target.value;
        tableSelectParams.district = tableDistrict;
        this.setState({tableParams: tableSelectParams});
        this.handleChangeSelectState(tableSelectParams);
        break;
    }
  };

  handleSelectVariableChange = (event) => {
    let district = this.state.selectParams.district;
    let selectParams = {};
    selectParams.variable = event.target.value;
    selectParams.district = district;
    this.setState({selectParams: selectParams});

    this.handleChangeSelectState(selectParams);
  };

  render() {
    const renderedChart = this.setRenderedChart(this.props.chartType);
    const showChart = this.props.chartType ? renderedChart : null;

    const renderSelectVariable = this.props.selectConfig && this.props.selectConfig.variable ?
      <div>
        Select param:
        <select value={this.props.selectValues.selectVariable} onChange={this.handleSelectVariableChange}>
          <option value="sum">Кл-во обьявлений</option>
          <option value="avg_price_sqm">Средняя цена м2</option>
          <option value="avg_price">Средняя цена квартиры</option>
          <option value="avg_sM">Средняя площадь квартиры</option>
          <option value="avg_lSM">Средняя жилая площадь</option>
          <option value="avg_kSM">Средняя площадь кухни</option>
        </select>
      </div>
      : null;

    const showSelectVariable = this.props.selectable ? renderSelectVariable : null;

    const renderSelectDistrict = this.props.selectConfig && this.props.selectConfig.district ?
      <div>
        Select dist:
        <select value={this.props.selectValues.selectDistrict} onChange={this.handleSelectDistrictChange}>
          <option value="0">Аркадия</option>
          <option value="1">Большой Фонтан</option>
          <option value="2">Киевский</option>
          <option value="3">Приморский</option>
          <option value="4">Малиновский</option>
          <option value="5">Суворовский</option>
          <option value="6">Молдаванка</option>
          <option value="7">Ильичевск</option>
        </select>
      </div>
      : null;

    const renderSelectTimestamp = this.props.selectConfig && this.props.selectConfig.timestamp ?
      <div>
        Select timestamp:
        <select value={this.props.selectValues.selectTimestamp} onChange={this.handleSelectTimestampChange}>
          {this.props.selectTimestampObject && this.props.selectTimestampObject.map(item => {
            return<option value={item.value}>{item.name}</option>
          })}
        </select>
      </div>
      : null;

    const showSelectDistrict = this.props.selectable ? renderSelectDistrict : null;

    const manageSelects = this.props.selectable ? <div>
      {showSelectDistrict} {showSelectVariable} {renderSelectTimestamp}
    </div> : null;

    const showContent = this.props.content ? (this.props.content /* || this.state.content */) : null;

    return <div>
      <h2>{this.props.header || this.state.header}</h2>
      {/* {this.props.chartType} will be rendered */}
      {manageSelects}
      {showChart}
      {showContent}
    </div>;
  }
}

export default GraphWidget;