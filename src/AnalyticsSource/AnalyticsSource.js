import React, { Component } from 'react';
import './AnalyticsSource.css';

// import data file
import realEstateData from '../data_source/real_estate_first';
import aggregatedData from '../data_source/computed_data';

import GraphWidget from '../GraphWidget/GraphWidget';

const defaultAllApartmentsData = {
    labels: ['1526077382', '1526805702', '1528805702', '1536805702'],
    data: [10000, 11000, 8500, 9200]
};

class AnalyticsSource extends Component {
  state = {
    showAllNumberArray: false,
    tableData: [{
      timeStamp : '90'
    }],
    selectParams: {
      district: 0,
      variable: 'sum'
    },
    pieParams: {
      district: 7,
      timestamp: 1544270458
    },
    tableParams: {
      district: 0,
      timestamp: 1544270458
    }
  };

  convertTimeStampToDate = (timestamp) => {
    let adjustedTS = timestamp * 1000;
    let dateObj = new Date(adjustedTS);
    const output = `${dateObj.getDate()}/${dateObj.getMonth()+1}/${dateObj.getFullYear()}`;
    return output;
  };

  getAggregatedData = () => {
    let aggrrr = aggregatedData;
    console.log("agragated data");
    console.log(aggrrr);
    this.setState({tableData: aggrrr});
  };

  getTimestampForSelect = () => {
    return aggregatedData.map(item => {
      return {
        value: item.timeStamp,
        name: this.convertTimeStampToDate(item.timeStamp)
      }
    });
  };

  componentWillMount() {
    this.getAggregatedData();
    this.getTimestampForSelect();
    setTimeout(() => this.setState({showAllNumberArray: true}), 800);
  }

  // generate districts Map
  generateDistrictsMap = (dataArray) => {
    let districtMap = new Map();
    for(let i = 0; i < dataArray.length; i++) {
      districtMap.set(dataArray[i].name, i);
    }
    // console.log(districtMap);
    return districtMap
  };

  // to prepare data for widget, convert to {data: [], labels: []} ;
  getDataForWidget = (districtId, variable) => {
    let chartData = {data: [], labels: []};
    let aggrrr = aggregatedData;
    // let districtsMap = this.generateDistrictsMap(aggregatedData[0].data);
    // let districtIndex = districtsMap.get(districtName);
    aggrrr.forEach(item => {
      chartData.labels.push(this.convertTimeStampToDate(item.timeStamp));
      chartData.data.push(item.data[districtId].general[variable]);
    });
    return chartData;
  };

  getPieChartData = (timestamp, districtId) => {
    let chartData = {data: [], labels: []};
    let singleTimestampObject = aggregatedData.filter(item => item.timeStamp == timestamp)[0];
    let singleDistrictArray = singleTimestampObject.data[districtId].grouped;
    for(let i = 0; i < singleDistrictArray.length; i++) {
      chartData.data.push(singleDistrictArray[i].sum);
      chartData.labels.push(`${i+1} rooms`);
    }
    return chartData;
  };

  getAllApartmentsData = () => {
    let apartmentLabels = [];
    defaultAllApartmentsData.labels.forEach(item => {
      apartmentLabels.push(this.convertTimeStampToDate(item));
    });
    return {labels: apartmentLabels,data: defaultAllApartmentsData.data};
  };

  // handle change select inside widget

  changeSelectedWidget = (selectionObject) => {
    this.setState({selectParams: selectionObject});
  };

  changePieParams = (pieParams) => {
    this.setState({pieParams: pieParams});
  };

  changeTableWidget = (selectionObject) => {
    this.setState({tableParams: selectionObject});
  };

  render() {
    const singleTableItem = this.state.tableData.filter(item => item.timeStamp == '1544270458');

    const tableItems = this.state.tableData.length > 0 ? singleTableItem.map(item => {
      return <div className="analyse-table">
        <p>{this.convertTimeStampToDate(item.timeStamp)}</p>
        <table>
          <tr>
            <th>Name</th>
            <th>Number</th>
            <th>AVG price m2</th>
            <th>AVG all m2</th>
            <th>AVG living m2</th>
            <th>AVG kitchen m2</th>
            <th>AVG price</th>
          </tr>
          {item.data.map(row => {
            return <tr>
              <td>{row.name}</td>
              <td>{row.general.sum}</td>
              <td>{row.general.avg_price_sqm}</td>
              <td>{row.general.avg_sM}</td>
              <td>{row.general.avg_lSM}</td>
              <td>{row.general.avg_kSM}</td>
              <td>{row.general.avg_price}</td>
            </tr>
          })}
        </table>
      </div>;
    }): null;

    const tablesWithData = this.state.showAllNumberArray ? <div>
      {tableItems}
    </div> : null;

    return <div>
      {/* // here go general statistics */}
      <div className="flex-grid">
        <GraphWidget
          header="Appartments"
          content="9500"
        />

        <GraphWidget
          header="Avarage price appt"
          content="$921"
        />

        <GraphWidget
          header="Avarage rent price"
          content="$420"
        />
      </div>

      {/* // here go all appartments graph */}
      <div className="flex-grid">
        <GraphWidget
          header="Total number of appartments on sale"
          chartType="bar"
          chartData={this.getAllApartmentsData()}
        />

        <GraphWidget
          header="Table statistics"
          chartType="table"
          tableData="data"
          selectable={true}
          selectConfig={{district: true, timestamp: true}}
          selectTimestampObject={this.getTimestampForSelect()}
          handleWidgetSelect={this.changeTableWidget}
          selectValues={{selectDistrict: this.state.tableParams.district, selectTimestamp: this.state.tableParams.timestamp}}
        />
      </div>

      {/* // here go graph widgets */}
      <div className="flex-grid">
        <GraphWidget
          header="Total number of appartments on sale"
          chartType="bar"
          selectable={true}
          selectConfig={{district: true, variable: true}}
          selectValues={{selectDistrict: this.state.selectParams.district, selectVariable: this.state.selectParams.variable}}
          handleWidgetSelect={this.changeSelectedWidget}
          chartData={this.getDataForWidget(this.state.selectParams.district, this.state.selectParams.variable)}
        />

        <GraphWidget
          header="Structure by rooms"
          chartType="pie"
          selectable={true}
          selectConfig={{district: true, timestamp: true}}
          selectTimestampObject={this.getTimestampForSelect()}
          selectValues={{selectDistrict: this.state.pieParams.district, selectTimestamp: this.state.pieParams.timestamp}}
          handleWidgetSelect={this.changePieParams}
          chartData={this.getPieChartData(this.state.pieParams.timestamp, this.state.pieParams.district)}
        />
      </div>
      <div className="flex-grid">
        <GraphWidget
          header="Testing line chart"
          chartType="line"
          chartData={this.getDataForWidget(2, 'avg_price_sqm')}
        />
        <GraphWidget
          header="Testing line chart"
          chartType="line"
          chartData={this.getDataForWidget(4, 'avg_price')}
        />
      </div>

      <div className="flex-grid">
        {tablesWithData}
      </div>
    </div>;
  }
}

export default AnalyticsSource;