const realEstateData = {
  totalNumber: [{"label":1526077382,"data":9762},{"label":1527924838,"data":10148},{"label":1528533026,"data":11848},{"label":1529141760,"data":11563},{"label":1529767047,"data":11405},{"label":1530434630,"data":10859},{"label":1530953652,"data":10492},{"label":1531687723,"data":9958},{"label":1532164862,"data":9308},{"label":1532766073,"data":9089},{"label":1533385053,"data":9006},{"label":1533986044,"data":8946},{"label":1534582082,"data":8717},{"label":1535812065,"data":8349},{"label":1536429156,"data":8506},{"label":1537301935,"data":8467},{"label":1537613551,"data":8774},{"label":1538252872,"data":8779},{"label":1539115413,"data":7400},{"label":1539546789,"data":8351},{"label":1540053965,"data":8942},{"label":1540646154,"data":8618},{"label":1541248006,"data":8457},{"label":1541972151,"data":7905},{"label":1542483034,"data":7243},{"label":1543073897,"data":6903},{"label":1543658110,"data":9033},{"label":1526805702,"data":9525},{"label":1527323395,"data":9650}],
  numberByRoom: [
    {
      label: '1 room',
      data: 1350
    }, {
      label: '2 rooms',
      data: 2400
    }, {
      label: '3 rooms',
      data: 1100
    }, {
      label: '4 rooms',
      data: 500
    }, {
      label: '5 and more',
      data: 60
    }],
  numberByDistrict: [
    {
      label: 'Приморский',
      data: 2500
    }, {
      label: 'Киевский',
      data: 2100
    }, {
      label: 'Суворовский',
      data: 1800
    }, {
      label: 'Малиновский',
      data: 1450
    }]
};

export default realEstateData;