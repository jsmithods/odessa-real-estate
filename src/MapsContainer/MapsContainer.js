import React, { Component } from 'react';
import './MapsContainer.css';

import MapsComponent from './MapsComponent/MapsComponent';

// import apartment data
import apartmentsList from '../data_source/flat_gps_info';
import rawImagePath from '../data_source/flat_images_info';

class MapsContainer extends Component {
  state = {
    filter: {
      byDistrict: ''
    },
    flats: [],
    imgByFlat: {},
    centerCoords: {lat: 46.48257, lng: 30.7233},
    mapZoom: 11,
    currentSlideImage: 0
  };

  componentWillMount() {
    const updatedList = apartmentsList.map(item => {
      const oldItem = item;
      oldItem.isOpen = false;
      return oldItem;
    });
    this.setState({flats: updatedList});
    this.getImgByFlat();
    // this.getMaxMinLatLng();
  };

  getMaxMinLatLng = (filteredList, setZoom = 13) => {
    const lats = filteredList.map(item => +item.lat);
    const lngs = filteredList.map(item => +item.lng);
    const finalLat = Math.min(...lats) + (Math.max(...lats) - Math.min(...lats))/2;
    const finalLng = Math.min(...lngs) + (Math.max(...lngs) - Math.min(...lngs))/2;

    const finalLatLng = {lat: finalLat, lng: finalLng};
    console.log(finalLatLng);
    this.setState({centerCoords: finalLatLng, mapZoom: setZoom})
  };

  getImgByFlat = () => {
    let output = {};
    rawImagePath.forEach(item => {
      let fl = item.flatId;
      if(output[fl] === undefined) {
        output[fl] = [];
      }
      output[fl].push(item.imageName);
    });
    this.setState({imgByFlat: output});
  };

  toggleHandleITM = (itemId) => {
    const newFlats = this.state.flats.map(item => {
      if(+item.uid === +itemId) {
        const oldState = item.isOpen;
        item.isOpen = !oldState;
      } else {
        item.isOpen = false
      }
      return item
    });
    this.setState({flats: newFlats});
    this.setState({currentSlideImage: 0})
  };

  filterList = (byDistrict) => {
    if(byDistrict) {
      let filteredList = apartmentsList.filter(item => {
        return item.districtName === byDistrict;
      });
      this.setState({flats: filteredList});
      this.getMaxMinLatLng(filteredList);
    } else {
      this.getMaxMinLatLng(apartmentsList, 11);
      this.setState({flats: apartmentsList});
    }
  };

  handleFilerDistrict = (event) => {
    let selectedValue = event.target.value;
    this.setState({filter: {byDistrict: selectedValue}});
    this.filterList(selectedValue);
  };

  handleMoveImage = (direction, uid) => {
    let oldImage = this.state.currentSlideImage;
    let imageLength = this.state.imgByFlat[uid].length;
    let nextImage = 0;
    if (direction === 'next') {
      nextImage = oldImage + 1;
    } else {
      nextImage = oldImage - 1;
    }
    if (nextImage === -1) {
      nextImage = imageLength - 1;
    }
    if (nextImage === imageLength) {
      nextImage = 0;
    }

    this.setState({currentSlideImage: nextImage})
  };

  handleChangeSlideItem = (direction, uid) => {
   console.log('inside parent handle change slide');
   console.log(direction);
   console.log(uid);

   this.handleMoveImage(direction, uid);
  };

  render() {

    const selectByDistrict = <div className="select-wrapper">
      <select value={this.state.filter.byDistrict} onChange={this.handleFilerDistrict}>
        <option value="">Show all</option>
        <option value="Аркадия">Аркадия</option>
        <option value="Киевский">Киевский</option>
        <option value="Приморский">Приморский</option>
        <option value="Малиновский">Малиновский</option>
        <option value="Суворовский">Суворовский</option>
      </select>
    </div>;

    return <div className="maps-component-wrapper">
      <div className="maps-filter-by">
        <p>by district: {selectByDistrict}</p>
      </div>
      <MapsComponent
        flats={this.state.flats}
        imagesList={this.state.imgByFlat}
        toggleHandleItem={this.toggleHandleITM}
        changeSlideShowItem={this.handleChangeSlideItem}
        mapCenter={this.state.centerCoords}
        mapZoom={this.state.mapZoom}

        currentSlideImage={this.state.currentSlideImage}
      />
    </div>;
  }
}

export default MapsContainer;