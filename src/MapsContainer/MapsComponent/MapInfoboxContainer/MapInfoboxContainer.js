import React, { Component } from 'react';
import './MapInfoboxContainer.css';

import { Link } from "react-router-dom";

class MapInfoboxContainer extends Component {
  state = {
    showContent: false
  };
  handleMoveImage = (direction) => {
    console.log(direction);
  };

  componentDidMount() {
    setTimeout(() => this.setState({showContent: true}), 500);
  }

  render() {
    console.log(this.props);

    const showInfobox = this.state.showContent ?
      <div className="map-infobox-container">
        <div className="map-slideshow-container">
          <img src={`http://localhost:9595/static/img/${this.props.marker.uid}/${this.props.marker.uid}-${this.props.marker.imagesList[this.props.marker.uid][0]}.jpg`}  alt="photo"/>
          <a className="prev" onClick={() => this.handleMoveImage('prev')} >&#10094;</a>
          <a className="next" onClick={() => this.handleMoveImage('next')} >&#10095;</a>
        </div>
        <Link to={`list/${this.props.marker.uid}`}>
          <p>{`${this.props.marker.streetName}`}</p>
        </Link>
        <p>{`${this.props.marker.rooms}к - ${this.props.marker.sM}m2 - ${this.props.marker.floor} этаж`}</p>
        <p>{`$ ${this.props.marker.price}`}</p>
      </div> : null;

    return {showInfobox};
  }
}

export default MapInfoboxContainer;