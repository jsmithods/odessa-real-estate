import React  from 'react';
import { Link } from "react-router-dom";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");
const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");
const { compose, withProps, withHandlers, withStateHandlers } = require("recompose");

/*const MapsComponent = withScriptjs(withGoogleMap((props) => {
  const flatMarkers = props.flats.map(item => {
    return <Marker key={item.uid} position={{lat: +item.lat, lng: +item.lng}}/>
  });

  return (
    <GoogleMap
      defaultZoom={10}
      defaultCenter={{lat: 46.4825, lng: 30.7233}}
    >
      {flatMarkers}
    </GoogleMap>
  );
  }
));*/

const dafaultMapCenter = {lat: 46.48257, lng: 30.7233};

const MapsComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBiPir8rcah7cp4viPklJ_RVy8l86IiisI&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `720px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withHandlers({
    onMarkerClustererClick: () => (markerClusterer) => {
      const clickedMarkers = markerClusterer.getMarkers();
      // console.log(`Current clicked markers length: ${clickedMarkers.length}`);
      // console.log(clickedMarkers)
    },
    getCurrentImage: () => () => {
      return 3
    },
    /*handleMoveImage: () => (direction) => {
      console.log(direction);
      props.changeSlideShowItem(direction)
    }*/
  }),
  withStateHandlers(() => ({
    isOpen: false,
    currentImage: 0
  }), {
    onToggleOpen: ({ isOpen }) => () => ({
      isOpen: !isOpen,
    }),
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={10}
    zoom={props.mapZoom}
    defaultCenter={dafaultMapCenter}
    center={props.mapCenter}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={60}
    >
      {props.flats.map(marker => (
        <Marker
          onClick={() => props.toggleHandleItem(marker.uid)}
          key={marker.uid}
          position={{ lat: +marker.lat, lng: +marker.lng }}
          /*icon="https://image.flaticon.com/icons/png/128/2/2144.png"*/
        >
          {marker.isOpen && <InfoBox
            onCloseClick={() => props.toggleHandleItem(marker.uid)}
            options={{ closeBoxURL: ``, enableEventPropagation: true }}
          >
            <div style={{ backgroundColor: '#fff', padding: `10px` }}>

              <div className="map-infobox-container">
                <div className="map-slideshow-container">
                  <img src={`http://localhost:9595/static/img/${marker.uid}/${marker.uid}-${props.imagesList[marker.uid][props.currentSlideImage]}.jpg`}  alt="photo"/>
                  <a className="prev" onClick={() => props.changeSlideShowItem('prev', marker.uid)} >&#10094;</a>
                  <a className="next" onClick={() => props.changeSlideShowItem('next', marker.uid)} >&#10095;</a>
                </div>
                <Link to={`list/${marker.uid}`}>
                  <p>{`${marker.streetName}`}</p>
                </Link>
                <p>{`${marker.rooms}к - ${marker.sM}m2 - ${marker.floor} этаж`}</p>
                <p>{`$ ${marker.price}`}</p>
              </div>

            </div>
          </InfoBox>}
        </Marker>
      ))}
    </MarkerClusterer>
  </GoogleMap>
);

export default MapsComponent;



