import React, { Component } from 'react';
import './AdminLayoutNavigation.css';

import { Link } from "react-router-dom";

class AdminLayoutNavigation extends Component {
  render() {
    return <nav>

      <header>
        <span></span>
        Admin Admin
        <a></a>
      </header>

      <ul>
        <li><span>Navigation</span></li>
        <li><Link to="/">Dashboard</Link></li>
        <li><Link to="/maps">Maps</Link></li>
        <li><Link to="/list">List</Link></li>
        <li><Link to="/ideas">Ideas</Link></li>
        <li><span>Other</span></li>
        <li><a>Search</a></li>
        <li><a>Settings</a></li>
        <li><a>Logout</a></li>
      </ul>

    </nav>;
  }
}

export default AdminLayoutNavigation;