import React, { Component } from 'react';
import './AdminLayout.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

import AnalyticsSource from '../AnalyticsSource/AnalyticsSource';
import AdminLayoutNavigation from "./AdminLayoutNavigation/AdminLayoutNavigation";

import MapsContainer from '../MapsContainer/MapsContainer';
import ListContainer from '../ListContainer/ListContainer';
import SingleFlat from '../ListContainer/SingleFlat/SingleFlat';
import IdeasContainer from '../IdeasContainer/IdeasContainer';

// import GraphWidget from '../GraphWidget/GraphWidget';

class AdminLayout extends Component {
  state = {
    dashboardHeader: 'Own project'
  };
  render() {
    return <Router>
      <div className="wrapper">

        <AdminLayoutNavigation />
        <main>
          <h1>{this.state.dashboardHeader}</h1>

          {/* Some basic analytics */}
          {/*<div className="flex-grid">*/}
            {/*<GraphWidget*/}
              {/*header="Header from Props"*/}
              {/*content="Content from Props"*/}
            {/*/>*/}
            {/*<GraphWidget/>*/}
          {/*</div>*/}

          <Route exact path="/"  component={AnalyticsSource} />
          <Route exact path="/maps"  component={MapsContainer} />

          <Route exact path="/list" component={ListContainer} />
          <Route exact path={'/list/:flatId'} component={SingleFlat} />

          <Route exact path="/ideas"  component={IdeasContainer} />



          {/*<div className="flex-grid">*/}
            {/*<div>*/}
              {/*<h2>Headline</h2>*/}
              {/*Some Content*/}
            {/*</div>*/}
            {/*<div>*/}

              {/*Some Content*/}
            {/*</div>*/}
          {/*</div>*/}

          {/*<div className="flex-grid">*/}
            {/*<div>*/}
              {/*<h2>Headline</h2>*/}
              {/*Some Content*/}
            {/*</div>*/}
          {/*</div>*/}
        </main>

      </div>
    </Router>;
  }
}

export default AdminLayout;